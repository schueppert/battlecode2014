package alpha;

import battlecode.common.Clock;
import battlecode.common.GameConstants;
import battlecode.common.GameObject;
import battlecode.common.RobotController;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.MapLocation;
import battlecode.common.RobotType;
import battlecode.common.Team;
import battlecode.common.*;

import java.lang.System;
import java.util.Set;
import java.util.HashSet;

public class RobotPlayer {

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // VARIABLE DECLARATIONS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static RobotController rc;
    static MapLocation myHQ;
    static MapLocation enemyHQ;
    static MapLocation pasture;
    static Team myTeam;
    static Team enemyTeam;
    static Set backtrack = new HashSet(500);
    static int handed;
    static int birthday;

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // RUN LOOP
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static void run(RobotController myRC) {
        rc = myRC;
        myHQ = rc.senseHQLocation();
        enemyHQ = rc.senseEnemyHQLocation();
        myTeam = rc.getTeam();
        enemyTeam = myTeam.opponent();
        while (true) {
            try {
                if (rc.getType() == RobotType.HQ) {
                    runHeadquarters();
                } else if (rc.getType() == RobotType.SOLDIER) {
                    runSoldier();
                } else if (rc.getType() == RobotType.PASTR) {
                    runPASTR();
                }
            } catch (Exception e) {
                System.out.println("Caught exception before it killed us:");
                e.printStackTrace();
            }
        }

    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // HEADQUARTERS CODE
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static void runHeadquarters() throws GameActionException {
        Direction spawnDirection = findShortestPath(myHQ.directionTo(enemyHQ));
        rc.broadcast(0, locToInt(myHQ)); //pasture location
        rc.broadcast(1, 1); //handedness flag
        rc.broadcast(2, 0); //pasture calculated
        rc.broadcast(3,0); //construction flag
        rc.spawn(spawnDirection);
        pasture = findBestPasture();
        rc.broadcast(0, locToInt(pasture));
        rc.broadcast(2, 1);
        while (true) {
            GameObject[] enemy = rc.senseNearbyGameObjects(Robot.class,15,enemyTeam);
            if (enemy.length>0&&rc.isActive()){
                rc.attackSquare(rc.senseLocationOf(enemy[0]));
            }
            if (rc.isActive() && rc.senseRobotCount() < GameConstants.MAX_ROBOTS) {
                spawnDirection = findShortestPath(myHQ.directionTo(enemyHQ));
                rc.spawn(spawnDirection);
            }
            rc.yield();
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PASTR CODE
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static void runPASTR() throws GameActionException {
        rc.broadcast(0, locToInt(rc.getLocation()));
        while (true) {
            rc.yield();
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SOLDIER CODE
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static void runSoldier() throws GameActionException {
        int distanceToPasture;
        Direction moveDirection;
        handed = rc.readBroadcast(1);
        birthday = Clock.getRoundNum();
        rc.broadcast(1, handed * -1);
        while (true) {
            pasture = intToLoc(rc.readBroadcast(0));
            distanceToPasture = rc.getLocation().distanceSquaredTo(pasture);
            if (Clock.getRoundNum() - birthday > backtrack.size() + 20) {
                backtrack.clear();
                birthday = Clock.getRoundNum();
            }
            if (rc.isActive() && distanceToPasture < 2 && rc.readBroadcast(2) == 1 && rc.sensePastrLocations(myTeam).length == 0 && Clock.getRoundNum()-rc.readBroadcast(3)>50) {
                rc.broadcast(3,Clock.getRoundNum());
                rc.construct(RobotType.PASTR);
            }
            if (rc.isActive() && distanceToPasture < 5 && rc.sensePastrLocations(myTeam).length > 0) {
                standGuard();
            }
            if (rc.isActive()) {
                if (distanceToPasture < 6 || rc.sensePastrLocations(myTeam).length == 0 || backtrack.size()<3) {
                    moveDirection = findShortestPath(rc.getLocation().directionTo(pasture));
                } else {
                    moveDirection = findHerdingPath(rc.getLocation().directionTo(pasture));
                }
                if (distanceToPasture > 6) {
                    rc.move(moveDirection);
                } else {
                    rc.sneak(moveDirection);
                }
                backtrack.add(rc.getLocation().hashCode());
            }
            rc.yield();
        }
    }

    static void standGuard() throws GameActionException{
        while (true) {
            GameObject[] enemy = rc.senseNearbyGameObjects(Robot.class,10,enemyTeam);
            if (enemy.length>0&&rc.isActive()){
                rc.attackSquare(rc.senseLocationOf(enemy[0]));
            }
            rc.yield();
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // UTILITIES
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    static int locToInt(MapLocation m) {
        return (m.x * 100 + m.y);
    }

    static MapLocation intToLoc(int i) {
        return new MapLocation(i / 100, i % 100);
    }

    static Direction findShortestPath(Direction d) {
        while (true) {
            if (rc.canMove(d) && !backtrack.contains(rc.getLocation().add(d).hashCode())) {
                return d;
            }
            if (handed == 1) {
                d = d.rotateLeft();
            } else {
                d = d.rotateRight();
            }
        }
    }

    static Direction findHerdingPath(Direction d) {
        if (handed == 1) {
            d = findShortestPath(d.rotateRight().rotateRight());
        } else {
            d = findShortestPath(d.rotateLeft().rotateLeft());
        }
        return d;
    }

    static MapLocation findBestPasture() {
        MapLocation bestLocation = myHQ;
        double bestscore = 0;
        double[][] growth = rc.senseCowGrowth();
        for (int x = 1; x < rc.getMapWidth() - 1; x++) {
            for (int y = 1; y < rc.getMapHeight() - 1; y++) {
                double score = growth[x][y];
                score += growth[x - 1][y - 1];
                score += growth[x - 1][y];
                score += growth[x - 1][y + 1];
                score += growth[x][y - 1];
                score += growth[x][y + 1];
                score += growth[x + 1][y - 1];
                score += growth[x + 1][y];
                score += growth[x + 1][y + 1];
                score = score * 5;
                score += Math.abs(x - enemyHQ.x) + Math.abs(y - enemyHQ.y);
                score -= Math.abs(x - myHQ.x) + Math.abs(y - myHQ.y);
                if (score >= bestscore) {
                    bestscore = score;
                    bestLocation = new MapLocation(x, y);
                }
            }
        }
        return bestLocation;
    }


}